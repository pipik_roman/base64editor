package sk.piro.scala.apps.b64Editor

import java.awt.Font
import javax.swing.JFrame
import javax.swing.JDialog
import javafx.embed.swing.JFXPanel
import java.awt.Dimension
import javax.swing.WindowConstants
import javafx.scene.Scene
import javafx.scene.layout.VBox
import javafx.scene.control.Label
import javafx.application.Platform
import javax.swing.SwingUtilities
import java.awt.Color

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 *
 */
class AboutApplicationDialog(editor : Base64Editor) extends JDialog(editor.frame, true) {
  val panel = new JFXPanel
  getContentPane.add(panel)
  val vBox = new VBox
  val m = editor.getMessages()
  vBox.setStyle(Base64Editor.STYLE_BACKGROUND_BEIGE)
  panel.setScene(new Scene(vBox, Base64Editor.WIDTH, Base64Editor.HEIGHT))

  val aboutL = new Label(m.get("Application.about"), Base64Editor.LOGO)
  val poweredL = new Label(m.get("Application.poweredBy"))
  vBox.getChildren().addAll(aboutL, poweredL)

  setIconImage(Base64Editor.ICON_APPLICATION_SWING)
  setTitle(editor.getMessages().aboutApplication())
  setPreferredSize(new Dimension(500, 70))
  setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)
  setResizable(false)

  //SwingUtils.packCropAndCenter(this)
  pack();
  setVisible(true)
}