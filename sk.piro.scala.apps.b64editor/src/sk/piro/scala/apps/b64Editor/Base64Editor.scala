/**
 * Package for Base64Editor
 */
package sk.piro.scala.apps.b64Editor
import javax.swing._
import java.awt.BorderLayout
import java.io._
import java.nio.charset.Charset
import javafx.embed.swing.JFXPanel
import javafx.scene.control.TextArea
import javafx.application.Platform
import javafx.scene.Scene
import javafx.scene.Group
import javafx.scene.layout.VBox
import javafx.scene.control.Label
import java.awt.Dimension
import javafx.scene.input.KeyEvent
import javafx.event.EventHandler
import sk.piro.codec.Base64Codec
import javafx.scene.input.InputMethodEvent
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.scene.control.TextField
import sk.piro.codec.CodecException
import javafx.scene.shape.Rectangle
import javafx.scene.paint.Color
import javafx.scene.text.TextAlignment
import javafx.scene.control.ContentDisplay
import javafx.application.Application
import javafx.stage.Stage
import sk.piro.utils.ExceptionUtils
import javax.imageio.ImageIO
import javafx.scene.control.MenuBar
import javafx.scene.control.MenuItem
import javafx.scene.control.Menu
import javafx.event.ActionEvent
import sk.piro.msg.DefaultMessages
import javafx.scene.image.Image
import javafx.scene.image.ImageView

/**
 * Simple base64 text editor with use of JavaFX
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
class Base64Editor extends Application() {
  //frame, because JavaFX doesn't allow me to set window icon
  var lang = 'en
  val messages = getMessages()
  val frame = new JFrame
  val panel = new JFXPanel
  frame.add(panel)
  frame.setPreferredSize(new Dimension(Base64Editor.WIDTH, Base64Editor.HEIGHT))
  frame.setIconImage(Base64Editor.ICON_APPLICATION_SWING)
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)

  //basic components
  val menuBar = new MenuBar
  val menuApplication = new Menu("", Base64Editor.ICON_APPLICATION_JAVAFX)
  val menuLanguage = new Menu("")
  menuBar.getMenus().addAll(menuApplication, menuLanguage)
  val menuApplicationAbout = new MenuItem("", Base64Editor.ICON_ABOUT)
  val menuApplicationClose = new MenuItem("", Base64Editor.ICON_CLOSE)
  menuApplication.getItems().addAll(menuApplicationAbout, menuApplicationClose)

  val menuLanguageSK = new MenuItem(getMessages('sk).languageName(), getFlag('sk))
  val menuLanguageEN = new MenuItem(getMessages('en).languageName(), getFlag('en))
  menuLanguage.getItems().addAll(menuLanguageSK, menuLanguageEN)

  val vBox = new VBox
  vBox.setStyle(Base64Editor.STYLE_BACKGROUND_BEIGE)
  val decodedL = new Label
  val encodedL = new Label
  val decodedTA = new TextArea
  decodedTA.setPrefColumnCount(25)
  decodedTA.setPrefRowCount(25)
  val encodedTA = new TextArea
  encodedTA.setPrefRowCount(25)
  val stateL = new Label()
  stateL.setContentDisplay(ContentDisplay.RIGHT)
  val codec = Base64Codec.getInstance()
  val green = new Rectangle(50000, 10)
  green.setFill(Color.GREENYELLOW)
  val red = new Rectangle(50000, 10)
  red.setFill(Color.RED)
  val blue = new Rectangle(50000, 10)
  blue.setFill(Color.DODGERBLUE)

  //set initial state
  setInit
  setLanguage()

  //events
  encodedTA.textProperty().addListener(new ChangeListener[String]() {
    override def changed(value : ObservableValue[_ <: String], s2 : String, s3 : String) : Unit = {
      if (!encodedTA.isFocused) {
        return
      }
      val encoded = value.getValue

      try {
        val decoded = codec.decode(encoded)
        decodedTA.setText(decoded)
        if (!encoded.equals(codec.encode(decoded))) {
          throwException(encoded)
        }
        if (encoded.isEmpty) {
          setInit
        } else {
          setOk
        }
      } catch {
        case e : CodecException => {
          setError()
        }
        case e : Throwable => {
          setError(ExceptionUtils.toString(e));
        }
      }
    }
  })

  decodedTA.textProperty().addListener(new ChangeListener[String]() {
    override def changed(value : ObservableValue[_ <: String], s2 : String, s3 : String) : Unit = {
      if (!decodedTA.isFocused) {
        return
      }
      val decoded = value.getValue
      try {
        val encoded = codec.encode(decoded)
        encodedTA.setText(encoded)
        if (!decoded.equals(codec.decode(encoded))) {
          throwException(decoded)
        }
        if (decoded.isEmpty) {
          setInit
        } else {
          setOk
        }
      } catch {
        case e : CodecException => {
          setError()
        }
        case e : Throwable => {
          setError(ExceptionUtils.toString(e))
        }
      }
    }
  })

  menuApplicationAbout.setOnAction(new EventHandler[ActionEvent]() {
    override def handle(event : ActionEvent) = {
      Platform.runLater(new Runnable() {
        override def run() = {
          val dialog = new AboutApplicationDialog(Base64Editor.this)
          SwingUtilities.invokeLater(new Runnable() {
            override def run() = {
              dialog.setVisible(true)
            }
          })
        }
      })
    }
  })

  menuApplicationClose.setOnAction(new EventHandler[ActionEvent]() {
    override def handle(event : ActionEvent) = {
      frame.dispose
    }
  })

  menuLanguageSK.setOnAction(new EventHandler[ActionEvent]() {
    override def handle(event : ActionEvent) = {
      setLanguage('sk)
    }
  })

  menuLanguageEN.setOnAction(new EventHandler[ActionEvent]() {
    override def handle(event : ActionEvent) = {
      setLanguage('en)
    }
  })

  def setOk() : Unit = {
    stateL setGraphic green
  }

  def setError(message : String = "") : Unit = {
    stateL setGraphic red
    if (!message.isEmpty()) {
      println(message)
      stateL setText message
    }
  }

  def setInit() : Unit = {
    stateL setGraphic blue
  }

  def throwException(input : String) = {
    throw new CodecException(getMessages().get("InvalidInput", input), input)
  }

  def start(stage : Stage) : Unit = {
    panel.setScene(new Scene(vBox, Base64Editor.WIDTH, Base64Editor.HEIGHT))
    vBox.getChildren.addAll(menuBar, decodedL, decodedTA, encodedL, encodedTA, stateL)
    frame.pack();
    //SwingUtils.packCropAndCenter(frame)
    frame.setVisible(true)
  }

  def getFlag(lang : Symbol = this.lang) = {
    val image = new ImageView()
    lang match {
      case 'sk => image.setImage(Base64Editor.SK_FLAG)
      case 'en => image.setImage(Base64Editor.EN_FLAG)
    }
    image
  }

  def getMessages(lang : Symbol = this.lang) : DefaultMessages = {
    lang match {
      case 'sk => Base64Editor.SK_MESSAGES
      case 'en => Base64Editor.EN_MESSAGES
    }
  }

  def setLanguage(lang : Symbol = this.lang) = {
    this.lang = lang
    val m = getMessages(lang)
    frame.setTitle(m.get("Application.name"))
    menuLanguage.setGraphic(getFlag())
    menuLanguage.setText(m.language())
    menuApplication.setText(m.application())
    menuApplicationAbout.setText(m.aboutApplication())
    menuApplicationClose.setText(m.close())

    decodedL.setText(m.get("DecodedText"))
    encodedL.setText(m.get("EncodedText"))
    decodedTA.setText(m.get("InsertTextToEncode"))
    encodedTA.setText(m.get("InsertTextToDecode"))
  }
}

object Base64Editor {
  val CLASS = classOf[Base64Editor]
  val SK_FLAG = new Image(CLASS.getResourceAsStream("sk.png"), 16, 16, true, true)
  val EN_FLAG = new Image(CLASS.getResourceAsStream("en.png"), 16, 16, true, true)
  val SK_MESSAGES = DefaultMessages.getInstance("sk", false, CLASS.getResourceAsStream("sk.properties"))
  val EN_MESSAGES = DefaultMessages.getInstance("en", false, CLASS.getResourceAsStream("en.properties"))
  val PURPLE = Color.rgb(0x71, 0x09, 0xAA)
  val GREEN = Color.rgb(0x9F, 0xEE, 0x00)
  val YELLOW = Color.rgb(0xFF, 0xD3, 0x00)
  val WIDTH = 500
  val HEIGHT = 300
  val STYLE_BACKGROUND_BEIGE = "-fx-background-color: beige"

  val ICON_APPLICATION_SWING = ImageIO.read(CLASS.getResourceAsStream("logo.png"))
  val ICON_APPLICATION_JAVAFX = new ImageView(new Image(CLASS.getResourceAsStream("logo.png"), 16, 16, true, true))
  val ICON_ABOUT = new ImageView(new Image(CLASS.getResourceAsStream("about.png"), 16, 16, true, true))
  val ICON_CLOSE = new ImageView(new Image(CLASS.getResourceAsStream("close.png"), 16, 16, true, true))

  private val ARC = 10
  private val RECT_SIZE = 10
  private val RECT_HALF = RECT_SIZE / 2

  private val R1 = new Rectangle(0, 0, RECT_SIZE, RECT_SIZE)
  R1.setFill(Base64Editor.PURPLE)
  R1.setArcHeight(ARC)
  R1.setArcWidth(ARC)
  private val R2 = new Rectangle(0, RECT_SIZE, RECT_SIZE, RECT_SIZE)
  R2.setFill(Base64Editor.GREEN)
  private val R3 = new Rectangle(RECT_HALF, RECT_HALF, RECT_SIZE, RECT_SIZE)
  R3.setFill(Base64Editor.YELLOW)
  R3.setArcHeight(ARC)
  R3.setArcWidth(ARC)
  val LOGO = new Group(R1, R2, R3)

  //launcher
  def main(args : Array[String]) : Unit = {
    Application.launch(CLASS)
  }
}